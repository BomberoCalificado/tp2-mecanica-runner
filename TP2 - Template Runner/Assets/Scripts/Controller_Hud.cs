﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    
    public Text distanceText;
    public Text Record;
    public Text gameOverText;
    public Slider Tiempo;
    public static bool gameOver = false;



    void Start()
    {

        distanceText.text = GameManager.Distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()

        
    {
        
        Tiempo.value = GameManager.Tiempodejuego;
        if (GameManager.Perdiste == true)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Press R to restart \n Total Distance: " + GameManager.Distance.ToString("f0");
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            GameManager.Distance += Time.deltaTime;
            distanceText.text = GameManager.Distance.ToString("f0");
            Record.text = GameManager.DistanciaRecord.ToString("f0");
        }
    }
}
