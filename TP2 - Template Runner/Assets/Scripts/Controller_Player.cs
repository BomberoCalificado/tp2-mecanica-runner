﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Player : MonoBehaviour
{
    public static bool Vunerable = true;
    public GameObject CampodeFuerza;
    public float ContadorPoder = 0;
    public bool PoderActivo;
    public float DuracionPoder = 0;
    public bool PoderEncendido = false;
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    private Animator animator;
    public bool PuedoSaltar;
    public Vector3 Gravedad;
    public TMPro.TMP_Text Tiempo10;
    public TMPro.TMP_Text Tiempo20;
    public TMPro.TMP_Text MonedasRecolectadas;
    private int cont;

    private void Start()
    {
        PuedoSaltar = false;
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        animator = GetComponent<Animator>();
        Physics.gravity = Gravedad;
        cont = 0;

    }

    void Update()
    {
        GetInput();

        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                animator.SetBool("Saltando", true);
            }
        }
        else
        {
            SeguirCorriendo();
        }

        if (PoderActivo == false)
        {
            ContadorPoder = ContadorPoder + Time.deltaTime;
        }
        if (ContadorPoder >= 8)
        {
            PoderActivo = true;
        }

        if (PoderActivo == true && Input.GetKeyDown(KeyCode.E))
        {
            CampodeFuerza.SetActive(true);
            PoderEncendido = true;
            
            
        }

        if (PoderEncendido == true)
        {
            DuracionPoder = DuracionPoder + Time.deltaTime;
            if (DuracionPoder >= 10)
            {
                CampodeFuerza.SetActive(false);
                ContadorPoder = 0;
                DuracionPoder = 0;
                PoderEncendido = false;
                PoderActivo = false;
            

            }
        }

        if (PoderEncendido == true)
        {
            Vunerable = false;
        }
        else Vunerable = true;

        setearTextos();
    }
    public void SeguirCorriendo()
    {
        animator.SetBool("ToqueSuelo", true);
        animator.SetBool("Saltando", false);
    }

    private void GetInput()
    {
        
        Duck();
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                    animator.SetBool("Deslizarse", true);
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                    animator.SetBool("Deslizarse", false);
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (Vunerable == true)
            {
                GameManager.Perdiste = true;
                Destroy(this.gameObject);
                Controller_Hud.gameOver = true;

            }
            
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }

        if (collision.gameObject.CompareTag("Tiempo+10"))
        {   
            GameManager.Tiempodejuego = GameManager.Tiempodejuego + 10;
            Tiempo10.gameObject.SetActive(true);

            Destroy(Tiempo10, 2);

        }
        
        if (collision.gameObject.CompareTag("Tiempo+20"))
        {
            GameManager.Tiempodejuego = GameManager.Tiempodejuego + 20;
            Tiempo20.gameObject.SetActive(true);
            Destroy(Tiempo20, 2);
        }

        if (collision.gameObject.CompareTag("Moneda"))
        {
            cont = cont + 1;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }

    private void setearTextos()
    {

        MonedasRecolectadas.text = " " + cont.ToString();

    }
}
