using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MENU : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ComenzarJuegoDelMenu()
    {

        SceneManager.LoadScene("Juego");

    }


    public void CreditosBoton()
    {
        SceneManager.LoadScene("Creditos");

    }

    public void Salir()
    {
        Application.Quit();
    }
}
