using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public static bool Perdiste;
    public GameObject Borrar;
    public static float Tiempodejuego = 100.0f;
    public static float Distance = 0;
    public static float DistanciaRecord = 10;
    public static float Monedas = 0;
    //public GameObject Personaje;
    //public GameObject GameOver;
    public class Jugadores 
    {
        string Nombre = "";
        float Puntaje = 0;
    };
    public static Jugadores Eljugador;
   



    void Start()
    {
        Gestordeaudio.instancia.ReproducirSonido("musica");
        Distance = 0;
        Perdiste = false;
        Monedas = 0;
        
        
    }

    void Update()
    {
        GestorPersistencia.instancia.data.PuntajeRecord = DistanciaRecord;
        if (Distance >= DistanciaRecord)
        {
            DistanciaRecord = Distance;
        }
        Tiempodejuego = Tiempodejuego - 5 * Time.deltaTime;
        if (Tiempodejuego <= 0)
        { Perdiste = true; }
        if (Perdiste == true)
        {
            GestorPersistencia.instancia.GuardarDataPersistencia();
           
            Gestordeaudio.instancia.PausarSonido("musica");
            Borrar.gameObject.SetActive(false);
            Time.timeScale = 0f;
        }

        if (Perdiste == true && Input.GetKeyDown(KeyCode.R))
        {
            GestorPersistencia.instancia.CargarDataPersistencia();
            Perdiste = false;
            SceneManager.LoadScene("Juego");
            Time.timeScale = 1;
            Distance = 0;
            Tiempodejuego = 100;
            Gestordeaudio.instancia.ReproducirSonido("musica");
           
        }

        
        
    }

    public void ComenzarJuegoDelMenu()
    {

        SceneManager.LoadScene("Juego");
       
    }


    public void CreditosBoton()
    {
        SceneManager.LoadScene("Creditos");

    }

    //public void GetInput()
    //{
    //    if (Input.GetKeyDown(KeyCode.R))
    //    {
    //        //Time.timeScale = 1;
    //        //Distance = 0;
    //        //Tiempodejuego = 100;
    //        SceneManager.LoadScene("Juego");
    //    }
    //}


}
