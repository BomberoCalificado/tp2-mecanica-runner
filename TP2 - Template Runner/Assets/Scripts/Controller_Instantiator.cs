﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> Items;
    public List<GameObject> Monedas;
    public GameObject instantiatePos;
    public GameObject Instanciaritemposicion;
    public GameObject InstanciarMonedaposicion;
    public GameObject InstanciarMonedaposicion2;
    public GameObject InstanciarMonedaposicion3;
    public GameObject InstanciarMonedaposicion4;
    public GameObject InstanciarMonedaposicion5;
    public GameObject InstanciarMonedaposicion6;
    public GameObject InstanciarMonedaposicion7;
    public GameObject InstanciarMonedaposicion8;
    public GameObject InstanciarMonedaposicion9;
    public float respawningTimer;
    private float time = 0;
   

    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    
    }

    void Update()
    {
        
        SpawnEnemies();
       
        ChangeVelocity();
    }
    
    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        // VELOCIDAD DE ENEMIGOS
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
        itemtiempo.Itemvelocity = Random.Range(1,3); 
        MonedaMovimiento.Monedavelocity = Random.Range(1, 0.50f);
    } 

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);

            Instantiate (Items[UnityEngine.Random.Range (0, Items.Count)], Instanciaritemposicion.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion2.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion3.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion4.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion5.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion6.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion7.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion8.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);

            Instantiate(Monedas[UnityEngine.Random.Range(0, Monedas.Count)], InstanciarMonedaposicion9.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);
        }
    }
}
