﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    public GameObject Fondo;
    public Vector3 Frenar;

    void Start()
        
    {
        Fondo = cam;
        startPos = transform.position.x;
        //length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        if (GameManager.Perdiste == false)
        {
            Activarefecto(); 
        }

        if (GameManager.Perdiste == true)
        {
            FrenarParalax();
        }

       

    }
    
    private void Activarefecto()
    {
        
            transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
            if (transform.localPosition.x < -20)
            {
                transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
            }
           
            Frenar = Fondo.transform.position; // GUARDAMOS LA ULTIMA POSICION DEL EFECTO



    }
    private void FrenarParalax () 
    {
        
        Fondo.transform.position = Frenar; // LA POSICION SERA LA ULTIMA GUARDADA DURANTE EL EFECTO
    }
}
