using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonedaMovimiento : MonoBehaviour
{
    public static float Monedavelocity;
    private Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(new Vector3(-Monedavelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
    }
    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            Destroy(this.gameObject);

        }
    }


}
