using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaPies : MonoBehaviour
{
    private Rigidbody rb;
    private bool floored;
    private float initialSize;
    private int i = 0;
    public Controller_Player logicaPersonaje1;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Duck();
    }

    private void OnTriggerStay(Collider other)
    {
        logicaPersonaje1.PuedoSaltar = true;
    }
    private void OnTriggerExit(Collider other)
    {
        logicaPersonaje1.PuedoSaltar = false;
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;

                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;

                }
            }
        }
    }
}

